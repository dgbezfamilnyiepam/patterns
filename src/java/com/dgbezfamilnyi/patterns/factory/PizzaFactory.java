package com.dgbezfamilnyi.patterns.factory;

public interface PizzaFactory {
	Pizza createPizza(PizzaType type);
}
