package com.dgbezfamilnyi.patterns.factory;

public interface PizzaStore {
	Pizza orderPizza(PizzaType type);
}
