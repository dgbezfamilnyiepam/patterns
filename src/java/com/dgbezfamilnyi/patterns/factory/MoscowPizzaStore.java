package com.dgbezfamilnyi.patterns.factory;

public class MoscowPizzaStore implements PizzaStore {
	private PizzaFactory pizzaFactory;

	public MoscowPizzaStore(PizzaFactory factory) {
		this.pizzaFactory = factory;
	}

	@Override
	public Pizza orderPizza(PizzaType type) {
		Pizza pizza = pizzaFactory.createPizza( type );

		pizza.prepare();
		pizza.bake();
		pizza.cut();
		pizza.box();

		return pizza;
	}
}
