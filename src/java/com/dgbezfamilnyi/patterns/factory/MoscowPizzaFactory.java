package com.dgbezfamilnyi.patterns.factory;

public class MoscowPizzaFactory implements PizzaFactory {
	@Override
	public Pizza createPizza(PizzaType type) {
		switch ( type ) {
			case CHEESE:
				return new CheesePizza();
			case PEPPERONI:
				return new PepperoniPizza();
			default:
				return null;
		}
	}
}
