package com.dgbezfamilnyi.patterns.factory;

public enum PizzaType {
	CHEESE, PEPPERONI
}
