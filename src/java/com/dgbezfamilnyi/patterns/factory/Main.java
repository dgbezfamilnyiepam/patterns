package com.dgbezfamilnyi.patterns.factory;

import static com.dgbezfamilnyi.patterns.factory.PizzaType.CHEESE;
import static com.dgbezfamilnyi.patterns.factory.PizzaType.PEPPERONI;

public class Main {
	public static void main(String[] args) {
		PizzaFactory factory = new MoscowPizzaFactory();
		PizzaStore store = new MoscowPizzaStore( factory );

		Pizza pizza = store.orderPizza( CHEESE );
		System.out.println( "We ordered a " + pizza.getName() + "\n" );

		pizza = store.orderPizza( PEPPERONI );
		System.out.println( "We ordered a " + pizza.getName() + "\n" );
	}
}
