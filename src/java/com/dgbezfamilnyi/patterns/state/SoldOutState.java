package com.dgbezfamilnyi.patterns.state;

public class SoldOutState implements State {
    private final GumballMachine gumballMachine;

    public SoldOutState(GumballMachine gumballMachine) {
        this.gumballMachine = gumballMachine;
    }

    @Override
    public void insertQuarter() {
        System.out.println("Eject your quarter, because no gumballs");
    }

    @Override
    public void ejectQuarter() {
        System.out.println("No quarter for eject");
    }

    @Override
    public void turnCrank() {
        System.out.println("No gumballs");
    }

    @Override
    public void dispense() {
        System.out.println("No gumball dispensed");
    }
}
