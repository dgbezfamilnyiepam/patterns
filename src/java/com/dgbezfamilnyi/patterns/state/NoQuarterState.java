package com.dgbezfamilnyi.patterns.state;

public class NoQuarterState implements State {
    private final GumballMachine gumballMachine;

    public NoQuarterState(GumballMachine gumballMachine) {
        this.gumballMachine = gumballMachine;
    }

    @Override
    public void insertQuarter() {
        System.out.println("You inserted quarter");
        gumballMachine.setState(gumballMachine.getHasQuarterState());
    }

    @Override
    public void ejectQuarter() {
        System.out.println("No quarter...");
    }

    @Override
    public void turnCrank() {
        System.out.println("You should insert quarter before turn crank");
    }

    @Override
    public void dispense() {
        System.out.println("No gumball dispensed");
    }
}
