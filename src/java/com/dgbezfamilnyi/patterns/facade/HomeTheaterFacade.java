package com.dgbezfamilnyi.patterns.facade;

public class HomeTheaterFacade {
    private Screen screen;
    private PopcornMachine popcornMachine;
    private AudioSystem audioSystem;

    public HomeTheaterFacade(Screen screen, PopcornMachine popcornMachine, AudioSystem audioSystem) {
        this.screen = screen;
        this.popcornMachine = popcornMachine;
        this.audioSystem = audioSystem;
    }

    public void turnOn() {
        popcornMachine.preparePopcorn();
        screen.turnOn();
        audioSystem.turnOn();
    }

    public void turnOff() {
        popcornMachine.clear();
        screen.turnOff();
        audioSystem.turnOff();
    }
}
