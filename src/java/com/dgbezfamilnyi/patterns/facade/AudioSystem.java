package com.dgbezfamilnyi.patterns.facade;

public class AudioSystem {
    public void turnOn() {
        System.out.println("Audio system was turned on");
    }

    public void turnOff() {
        System.out.println("Audio system was turned off");
    }
}
