package com.dgbezfamilnyi.patterns.facade;

public class Main {
    public static void main(String[] args) {
        HomeTheaterFacade homeTheater = Home.getHomeTheaterRemoteControl();
        homeTheater.turnOn();
        homeTheater.turnOff();
    }
}
