package com.dgbezfamilnyi.patterns.facade;

public class Screen {
    public void turnOn() {
        System.out.println("Screen was turned on");
    }

    public void turnOff() {
        System.out.println("Screen was turned off");
    }
}
