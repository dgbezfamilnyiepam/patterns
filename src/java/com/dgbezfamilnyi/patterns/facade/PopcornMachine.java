package com.dgbezfamilnyi.patterns.facade;

public class PopcornMachine {
    public void preparePopcorn() {
        System.out.println("Popcorn was prepared");
    }

    public void clear() {
        System.out.println("Popcorn machine was cleaned");
    }
}
