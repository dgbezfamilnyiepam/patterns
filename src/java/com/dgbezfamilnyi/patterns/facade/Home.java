package com.dgbezfamilnyi.patterns.facade;

public class Home {
    public static HomeTheaterFacade getHomeTheaterRemoteControl() {
        return new HomeTheaterFacade(new Screen(), new PopcornMachine(), new AudioSystem());
    }
}
