package com.dgbezfamilnyi.patterns.strategy;

public interface QuackBehavior {
    void quack();
}