package com.dgbezfamilnyi.patterns.strategy;

public class SteppeDuck extends Duck {

    public SteppeDuck() {
        this.flyBehavior = new FlyWithWings();
        this.quackBehavior = new Quack();
    }

    public void display() {
        System.out.println("I am Steppe duck!");
    }

}