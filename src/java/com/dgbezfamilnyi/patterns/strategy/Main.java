package com.dgbezfamilnyi.patterns.strategy;

public class Main {
    public static void main(String[] arg) {

        Duck rubberDuck = new RubberDuck();
        rubberDuck.display();
        rubberDuck.fly();
        rubberDuck.quack();

        Duck steppeDuck = new SteppeDuck();
        steppeDuck.display();
        steppeDuck.fly();
        steppeDuck.quack();

    }
}