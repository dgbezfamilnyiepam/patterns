package com.dgbezfamilnyi.patterns.strategy;

public interface FlyBehavior {
    void fly();
}