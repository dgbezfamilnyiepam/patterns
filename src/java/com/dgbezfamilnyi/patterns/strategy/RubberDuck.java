package com.dgbezfamilnyi.patterns.strategy;

public class RubberDuck extends Duck {

    public RubberDuck() {
        this.flyBehavior = new FlyNoWay();
        this.quackBehavior = new Squeak();
    }

    public void display() {
        System.out.println("I am Rubber duck");
    }

}