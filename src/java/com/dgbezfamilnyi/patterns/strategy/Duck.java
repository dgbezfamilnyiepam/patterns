package com.dgbezfamilnyi.patterns.strategy;

public abstract class Duck {
    protected FlyBehavior flyBehavior;
    protected QuackBehavior quackBehavior;

    public void quack() {
        if (quackBehavior != null) {
            quackBehavior.quack();
        }
    }

    public void fly() {
        if (flyBehavior != null) {
            flyBehavior.fly();
        }
    }

    abstract void display();
}