package com.dgbezfamilnyi.patterns.decorator;

public class Main {
	public static void main(String[] args) {
		Beverage latte = new IceCream( new Latte() );
		System.out.println( latte.getDescription()
									+ " $" + latte.cost() );

		Beverage espresso = new Milk( new Espresso() );
		System.out.println( espresso.getDescription()
									+ " $" + espresso.cost() );
	}
}
