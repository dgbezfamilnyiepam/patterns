package com.dgbezfamilnyi.patterns.decorator;

public class Milk extends Supplement {

	public Milk(Beverage beverage) {
		this.beverage = beverage;
	}

	@Override
	public String getDescription() {
		return beverage.getDescription() + ", Milk";
	}

	@Override
	public double cost() {
		return .10 + this.beverage.cost();
	}
}
