package com.dgbezfamilnyi.patterns.decorator;

public class Cinnamon extends Supplement {
	public Cinnamon(Beverage beverage) {
		this.beverage = beverage;
	}

	@Override
	public String getDescription() {
		return this.beverage.getDescription() + ", Cinnamon";
	}

	@Override
	public double cost() {
		return .10 + this.beverage.cost();
	}
}
