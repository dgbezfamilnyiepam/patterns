package com.dgbezfamilnyi.patterns.decorator;

public class Latte extends Beverage {
	public Latte() {
		this.description = "Latte";
	}

	@Override
	public double cost() {
		return 1.0;
	}
}
