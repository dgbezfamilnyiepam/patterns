package com.dgbezfamilnyi.patterns.decorator;

public abstract class Supplement extends Beverage {
	protected Beverage beverage;

	public abstract String getDescription();
}
