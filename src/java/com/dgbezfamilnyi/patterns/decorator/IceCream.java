package com.dgbezfamilnyi.patterns.decorator;

public class IceCream extends Supplement {
	public IceCream(Beverage beverage) {
		this.beverage = beverage;
	}

	@Override
	public String getDescription() {
		return this.beverage.getDescription() + ", Ice cream";
	}

	@Override
	public double cost() {
		return this.beverage.cost() + .10;
	}
}
