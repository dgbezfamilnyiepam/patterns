package com.dgbezfamilnyi.patterns.linker;

public class Waitress {
    private MenuComponent wholeMenu;

    public Waitress(MenuComponent menu) {
        this.wholeMenu = menu;
    }


    public void printMenu() {
        wholeMenu.print();
    }
}
