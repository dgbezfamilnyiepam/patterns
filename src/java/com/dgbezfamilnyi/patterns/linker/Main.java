package com.dgbezfamilnyi.patterns.linker;

public class Main {
    public static void main(String[] args) {
        //create all submenus
        MenuComponent pancakeHouseMenu = new Menu("PANCAKE HOUSE MENU", "Breakfast");
        MenuComponent dinerMenu = new Menu("DINER MENU", "Lunch");
        MenuComponent cafeMenu = new Menu("CAFE MENU", "Dinner");
        MenuComponent dessertMenu = new Menu("DESSERT MENU", "Dessert of course!");

        //create whole menu
        MenuComponent wholeMenu = new Menu("ALL MENUS", "All menus combined");
        wholeMenu.add(pancakeHouseMenu);
        wholeMenu.add(dinerMenu);
        wholeMenu.add(cafeMenu);

        //fill all submenus
        pancakeHouseMenu.add(new MenuItem("pancake", "very thinned pancake",
                true, 3.23));
        dinerMenu.add(new MenuItem("Pasta", "Spaghetti with marinara Sauce", false, 5.34));
        dinerMenu.add(dessertMenu);
        cafeMenu.add(new MenuItem("Soup of the day", "A cup of the soup of the day",
                false, 4.32));
        dessertMenu.add(new MenuItem("Apple pie", "Apple pie with vanilla cream",
                true, 1.23));

        Waitress waitress = new Waitress(wholeMenu);
        waitress.printMenu();
    }
}
