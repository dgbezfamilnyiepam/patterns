package com.dgbezfamilnyi.patterns.adapter;

import com.dgbezfamilnyi.patterns.adapter.someexternallibrary.ExternalLibraryOld;

public class Client {
	public static void main(String[] args) {
		ExternalLibraryOld externalLibrary = Factory.createExternalLibraryInstance();
		System.out.println( externalLibrary.doSomething() );
	}
}
