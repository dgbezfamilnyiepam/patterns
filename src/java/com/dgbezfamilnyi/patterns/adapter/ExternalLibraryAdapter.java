package com.dgbezfamilnyi.patterns.adapter;

import com.dgbezfamilnyi.patterns.adapter.someexternallibrary.ExternalLibraryNew;
import com.dgbezfamilnyi.patterns.adapter.someexternallibrary.ExternalLibraryOld;

public class ExternalLibraryAdapter implements ExternalLibraryOld {
	private ExternalLibraryNew externalLibrary;

	public ExternalLibraryAdapter(ExternalLibraryNew externalLibraryNew) {
		this.externalLibrary = externalLibraryNew;
	}

	@Override
	public String doSomething() {
		return externalLibrary.doSomething();
	}
}
