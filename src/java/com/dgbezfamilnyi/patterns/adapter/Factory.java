package com.dgbezfamilnyi.patterns.adapter;

import com.dgbezfamilnyi.patterns.adapter.someexternallibrary.ExternalLibraryNewImpl;
import com.dgbezfamilnyi.patterns.adapter.someexternallibrary.ExternalLibraryOld;

public class Factory {
	public static ExternalLibraryOld createExternalLibraryInstance() {
		return new ExternalLibraryAdapter( new ExternalLibraryNewImpl() );
	}
}
