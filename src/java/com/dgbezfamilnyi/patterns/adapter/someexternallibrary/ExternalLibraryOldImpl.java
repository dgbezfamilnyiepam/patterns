package com.dgbezfamilnyi.patterns.adapter.someexternallibrary;

public class ExternalLibraryOldImpl implements ExternalLibraryOld {
	@Override
	public String doSomething() {
		return "from old external library";
	}
}
