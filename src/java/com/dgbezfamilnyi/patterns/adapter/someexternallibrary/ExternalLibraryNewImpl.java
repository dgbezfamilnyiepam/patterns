package com.dgbezfamilnyi.patterns.adapter.someexternallibrary;

public class ExternalLibraryNewImpl implements ExternalLibraryNew {
	@Override
	public String doSomething() {
		return "from new external library";
	}
}
