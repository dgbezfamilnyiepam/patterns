package com.dgbezfamilnyi.patterns.patternMethod;

public class TeaBeverage extends CaffeineBeverage {
    @Override
    protected void brew() {
        System.out.println("Tea was brewed.");
    }

    @Override
    protected void addCondiments() {
        System.out.println("Add lemon and sugar");
    }

    @Override
    protected boolean customerWantsCondiments() {
        System.out.println("Never add condiment in tea case.");
        return false;
    }
}
