package com.dgbezfamilnyi.patterns.patternMethod;

public class Main {
    public static void main(String[] args) {
        CaffeineBeverage beverage = new CoffeeBeverage();
        beverage.prepare();

        beverage = new TeaBeverage();
        beverage.prepare();
    }
}
