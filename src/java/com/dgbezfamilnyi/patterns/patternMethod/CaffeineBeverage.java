package com.dgbezfamilnyi.patterns.patternMethod;

public abstract class CaffeineBeverage {
    public final void prepare() {
        boilWater();
        brew();
        pourInCup();
        if (customerWantsCondiments()) {
            addCondiments();
        }
    }

    private void boilWater() {
        System.out.println("Water was boiled");
    }

    private void pourInCup() {
        System.out.println("Beverage was poured in cup");
    }

    // interceptor method
    protected boolean customerWantsCondiments() {
        return true;
    }

    protected abstract void brew();

    protected abstract void addCondiments();
}
