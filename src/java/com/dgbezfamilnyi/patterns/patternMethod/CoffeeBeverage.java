package com.dgbezfamilnyi.patterns.patternMethod;

public class CoffeeBeverage extends CaffeineBeverage {
    @Override
    protected void brew() {
        System.out.println("Coffee was brewed.");
    }

    @Override
    protected void addCondiments() {
        System.out.println("Add milk and sugar to coffee.");
    }
}
