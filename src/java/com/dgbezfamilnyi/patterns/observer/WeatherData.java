package com.dgbezfamilnyi.patterns.observer;

import java.util.ArrayList;
import java.util.List;

public class WeatherData implements Subject {
    private static final int indexIfDoesNotContain = -1;
    private float temperature;
    private float humidity;
    private float pressure;
    private List<Observer> observers;

    public WeatherData() {
        observers = new ArrayList<>();
    }

    public float getTemperature() {
        return temperature;
    }

    public float getHumidity() {
        return humidity;
    }

    public float getPressure() {
        return pressure;
    }

    public void setMeasurements(float temperature, float humidity, float pressure) {
        this.temperature = temperature;
        this.humidity = humidity;
        this.pressure = pressure;
        notifyObservers();
    }

    public void registerObserver(Observer o) {
        if (observers.indexOf(o) == indexIfDoesNotContain) {
            observers.add(o);
        }
    }

    public boolean deleteObserver(Observer o) {
        return observers.remove(o);
    }

    public void notifyObservers() {
        for (Observer observer : observers) {
            observer.notifyMe();
        }
    }

}