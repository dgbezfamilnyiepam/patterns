package com.dgbezfamilnyi.patterns.observer;

public interface DisplayElement {
    String display();
}