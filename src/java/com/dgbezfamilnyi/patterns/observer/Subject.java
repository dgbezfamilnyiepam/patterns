package com.dgbezfamilnyi.patterns.observer;

public interface Subject {
    void registerObserver(Observer o);

    boolean deleteObserver(Observer o);

    void notifyObservers();
}