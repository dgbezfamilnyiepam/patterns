package com.dgbezfamilnyi.patterns.observer;

public class DisplayFullVersion implements Observer, DisplayElement {
    private WeatherData weatherData;
    private float temperature;
    private float humidity;
    private float pressure;

    public DisplayFullVersion(WeatherData weatherData) {
        this.weatherData = weatherData;
        weatherData.registerObserver(this);
    }

    public void setWeatherData(WeatherData weatherData) {
        this.weatherData = weatherData;
    }

    public WeatherData getWeatherData() {
        return weatherData;
    }

    public void notifyMe() {
        this.temperature = weatherData.getTemperature();
        this.humidity = weatherData.getHumidity();
        this.pressure = weatherData.getPressure();
        display();
    }

    public String display() {
        String stringForRender = "DisplayFullVersion : \n" +
                "Temperature is " + temperature + "\n" +
                "Humidity is " + humidity + "\n" +
                "Pressure is " + pressure;
        System.out.println(stringForRender);
        return stringForRender;
    }
}