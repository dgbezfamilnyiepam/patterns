package com.dgbezfamilnyi.patterns.observer;

public class Main {
    public static void main(String[] arg) {
        WeatherData weatherData = new WeatherData();
        DisplayElement displayFullVersion = new DisplayFullVersion(weatherData);
        DisplayElement displayShortVersion = new DisplayShortVersion(weatherData);

        weatherData.setMeasurements(8.8f, 88f, 88f);
        weatherData.setMeasurements(11.11f, 11f, 11f);
    }
}