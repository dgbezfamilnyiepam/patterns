package com.dgbezfamilnyi.patterns.observer;

public interface Observer {
    void notifyMe();
}