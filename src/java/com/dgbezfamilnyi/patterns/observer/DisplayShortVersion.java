package com.dgbezfamilnyi.patterns.observer;

public class DisplayShortVersion implements Observer, DisplayElement {
    private WeatherData weatherData;
    private float temperature;

    public DisplayShortVersion(WeatherData weatherData) {
        this.weatherData = weatherData;
        weatherData.registerObserver(this);
    }

    public void setWeatherData(WeatherData weatherData) {
        this.weatherData = weatherData;
    }

    public WeatherData getWeatherData() {
        return weatherData;
    }

    public void notifyMe() {
        this.temperature = weatherData.getTemperature();
        display();
    }

    public String display() {
        String stringForRender = "DisplayShortVersion \n" +
                "Temperature is " + temperature;
        System.out.println(stringForRender);
        return stringForRender;
    }
}