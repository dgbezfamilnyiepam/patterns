package com.dgbezfamilnyi.patterns.command;

import java.util.Arrays;

import com.dgbezfamilnyi.patterns.command.commands.Command;
import com.dgbezfamilnyi.patterns.command.commands.NoCommand;

public class RemoteControl {
	private Command[] onCommands;
	private Command[] offCommands;
	private Command lastCommand;


	public RemoteControl(int countOfSlots) {
		onCommands = new Command[countOfSlots];
		offCommands = new Command[countOfSlots];

		NoCommand noCommand = new NoCommand();
		for ( int i = 0; i < onCommands.length; i++ ) {
			onCommands[i] = noCommand;
			offCommands[i] = noCommand;
		}
		lastCommand = noCommand;
	}

	public RemoteControl(Command[] onCommands, Command[] offCommands) {
		this.onCommands = onCommands;
		this.offCommands = offCommands;
	}

	public RemoteControl() {
		this( 3 );
	}

	public void setCommand(int slot, Command onCommand, Command offCommand) {
		this.onCommands[slot] = onCommand;
		this.offCommands[slot] = offCommand;
	}

	public Command[] getOnCommands() {
		return onCommands;
	}

	public Command[] getOffCommands() {
		return offCommands;
	}

	public void pushOnButton(int slot) {
		onCommands[slot].execute();
		lastCommand = onCommands[slot];
	}

	public void pushOffButton(int slot) {
		offCommands[slot].execute();
		lastCommand = offCommands[slot];
	}

	public void undo() {
		lastCommand.undo();
	}

	@Override
	public String toString() {
		return "RemoteControl{" +
				"onCommands=" + Arrays.toString( onCommands ) +
				", offCommands=" + Arrays.toString( offCommands ) +
				'}';
	}
}
