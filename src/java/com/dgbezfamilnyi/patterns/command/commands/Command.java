package com.dgbezfamilnyi.patterns.command.commands;

public interface Command {
	void execute();
	void undo();
}
