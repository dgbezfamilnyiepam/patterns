package com.dgbezfamilnyi.patterns.command.commands;

import com.dgbezfamilnyi.patterns.command.consumers.Light;

public class LightOnCommand implements Command {
	private Light light;

	public LightOnCommand(Light light) {
		this.light = light;
	}

	@Override
	public void execute() {
		light.onLight();
	}

	@Override
	public void undo() {
		light.offLight();
	}

	@Override
	public String toString() {
		return "LightOnCommandObject";
	}
}
