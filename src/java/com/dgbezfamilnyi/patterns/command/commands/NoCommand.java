package com.dgbezfamilnyi.patterns.command.commands;

public class NoCommand implements Command {
	@Override
	public void execute() {
		System.out.println("You use NoCommand object!");
	}

	@Override
	public void undo() {
		System.out.println("You use NoCommand object!");
	}

	@Override
	public String toString() {
		return "NoCommandObject";
	}
}
