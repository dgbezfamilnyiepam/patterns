package com.dgbezfamilnyi.patterns.command.commands;

import com.dgbezfamilnyi.patterns.command.consumers.Door;

public class DoorCloseCommand implements Command {
	private Door door;

	public DoorCloseCommand(Door door) {
		this.door = door;
	}

	@Override
	public void execute() {
		door.close();
	}

	@Override
	public void undo() {
		door.open();
	}

	@Override
	public String toString() {
		return "DoorCloseCommandObject";
	}
}
