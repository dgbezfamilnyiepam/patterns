package com.dgbezfamilnyi.patterns.command.commands;

import com.dgbezfamilnyi.patterns.command.consumers.Light;

public class LightOffCommand implements Command {
	private Light light;

	public LightOffCommand(Light light) {
		this.light = light;
	}

	@Override
	public void execute() {
		light.offLight();
	}

	@Override
	public void undo() {
		light.onLight();
	}

	@Override
	public String toString() {
		return "LightOffCommandObject";
	}
}
