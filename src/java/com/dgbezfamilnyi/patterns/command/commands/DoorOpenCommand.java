package com.dgbezfamilnyi.patterns.command.commands;

import com.dgbezfamilnyi.patterns.command.consumers.Door;

public class DoorOpenCommand implements Command {
	private Door door;

	public DoorOpenCommand(Door door) {
		this.door = door;
	}

	@Override
	public void execute() {
		door.open();
	}

	@Override
	public void undo() {
		door.close();
	}

	@Override
	public String toString() {
		return "DoorOpenCommandObject";
	}
}
