package com.dgbezfamilnyi.patterns.command;

public class Human {
	public static void main(String[] args) {
		RemoteControl remoteControl = RemoteControlLoader.prepareStandardRemoteControl();
		System.out.println(remoteControl);

		remoteControl.pushOnButton( 0 );
		remoteControl.pushOnButton( 1 );
		remoteControl.undo();
		remoteControl.pushOnButton( 1 );
		remoteControl.pushOffButton( 0 );
		remoteControl.pushOffButton( 1 );
	}
}
