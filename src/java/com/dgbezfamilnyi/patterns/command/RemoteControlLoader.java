package com.dgbezfamilnyi.patterns.command;

import com.dgbezfamilnyi.patterns.command.commands.DoorCloseCommand;
import com.dgbezfamilnyi.patterns.command.commands.DoorOpenCommand;
import com.dgbezfamilnyi.patterns.command.commands.LightOffCommand;
import com.dgbezfamilnyi.patterns.command.commands.LightOnCommand;
import com.dgbezfamilnyi.patterns.command.consumers.Door;
import com.dgbezfamilnyi.patterns.command.consumers.Light;

public class RemoteControlLoader {
	public static RemoteControl prepareStandardRemoteControl() {
		Light lightOnKitchen = new Light( "Kitchen" );
		Door frontDoor = new Door( "Front" );

		LightOnCommand lightOnCommand = new LightOnCommand( lightOnKitchen );
		LightOffCommand lightOffCommand = new LightOffCommand( lightOnKitchen );
		DoorOpenCommand doorOpenCommand = new DoorOpenCommand( frontDoor );
		DoorCloseCommand doorCloseCommand = new DoorCloseCommand( frontDoor );

		RemoteControl remoteControl = new RemoteControl(2);
		remoteControl.setCommand( 0, lightOnCommand, lightOffCommand );
		remoteControl.setCommand( 1, doorOpenCommand, doorCloseCommand );
		return remoteControl;
	}
}
