package com.dgbezfamilnyi.patterns.command.consumers;

public class Door {
	private String location;

	public Door(String location) {
		this.location = location;
	}

	public void open() {
		System.out.println("Door opened in " + location);
	}

	public void close() {
		System.out.println("Door closed in " + location);
	}
}
