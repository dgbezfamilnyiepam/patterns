package com.dgbezfamilnyi.patterns.command.consumers;

public class Light {
	private String location;

	public Light(String location) {
		this.location = location;
	}

	public void onLight() {
		System.out.println("Light turned on in " + location);
	}

	public void offLight() {
		System.out.println("Light turned off in " + location);
	}
}
